﻿Shader "Custom/ShieldBubble" {
	Properties {
		_Color ("Color", Color) = (0.0, 1.0, 0.0, 1.0)
		_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1.0)
		_Shininess ("Shininess (0.01-1.0)", Range(0.01, 1)) = 0.075
		_Gloss ("Gloss (0.01-1.0)", Range(0.01, 1)) = 0.075
		_Inside ("Inside (0.0-2.0)", Range(0.0, 2.0) ) = 0.0
		_Rim ("Rim (0.0-2.0)", Range(0.0, 2.0)) = 1.2
		_Speed ("Speed (0.5-5.0)", Range(0.5, 5.0)) = 0.5
		_Tile("Tile (1.0-10.0)", Range(1.0,10.0)) = 5.0
		_Strength("Strength (0.0-5.0)", Range(0.0, 5.0)) = 1.5
		_texture ("Texture", 2D) = "white" {}
		_Cube ("Reflection Cubemap", Cube) = "black" { TexGen CubeReflect }
		_ReflectColor ("Reflection Color", Color) = (1,1,1,0.5)
	}
	SubShader {
		Tags { 
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent" 
		}
		
		Cull Back
		ZWrite On
		ZTest LEqual

		CGPROGRAM
		#pragma surface surf BlinnPhongEditor alpha
		#pragma target 3.0

		fixed4 _Color;
		sampler2D _CameraDepthTexture;
		fixed _Inside;
		fixed _Rim;
		sampler2D _Texture;
		samplerCUBE _Cube;
		fixed4 _ReflectColor;
		fixed _Speed;
		fixed _Tile;
		fixed _Strength;

		half _Shininess;
		half _Gloss;

		struct EditorSurfaceOutput {
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half3 Gloss;
			half Specular;
			half Alpha;
		};

		inline half4 LightingBlinnPhongEditor_PrePass (EditorSurfaceOutput s, half4 light){
			half3 spec = light.a * s.Gloss;

			half4 c;

			c.rgb = (s.Albedo * light.rgb + light.rgb * spec);

			c.a = s.Alpha + Luminance(spec);

			return c;
		}

		inline half4 LightingBlinnPhongEditor (EditorSurfaceOutput s, half3 lightDir, half3 viewDir, half atten){
			viewDir = normalize(viewDir);
			half3 h = normalize(lightDir + viewDir);

			half diff = max(0, dot(s.Normal, lightDir));

			float nh = max(0, dot(s.Normal, h));
			float3 spec = pow(nh, s.Specular*128.0) * s.Gloss;

			half4 res;
			res.rgb = _LightColor0.rgb * (diff * atten * 2.0);
			res.w = spec * Luminance (_LightColor0.rgb);

			return LightingBlinnPhongEditor_PrePass(s, res);
		}

		struct Input{
			float4 screenPos;
			float3 viewDir;
			float2 uv_Texture;

			//float3 worldRefl;
		};

		void vert (inout appdata_full v, out Input o){
			
		}

		void surf (Input IN, inout EditorSurfaceOutput o){
			o.Albedo = fixed3(0.0, 0.0, 0.0);
			o.Normal = fixed3(0.0, 0.0, 1.0);
			o.Emission = 0.0;
			o.Gloss = _Gloss;
			o.Specular = _Shininess;
			o.Alpha = 1.0;
			float4 ScreenDepthDiff0 = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(IN.screenPos)).r) - IN.screenPos.z;
			float4 Saturate0 = fixed4(0.3,0.3,0.3,1.0);
			float4 Fresnel10_1_NoInput = fixed4(0,0,1,1);
			float4 Fresnel10 = float4(1.0 - dot(normalize(float4(IN.viewDir, 1.0).xyz), normalize(Fresnel10_1_NoInput.xyz)));
			float4 Step0 = step(Fresnel10,float4(1.0));
			float4 Clamp0 = clamp(Step0, _Inside.xxxx, float4(1.0));
			float4 Pow0 = pow(Fresnel10, _Rim.xxxx);
			float4 Multiply5 = _Time * _Speed.xxxx;
			float4 UV_Pan0 = float4((IN.uv_Texture.xyxy).x, (IN.uv_Texture.xyxy).y + Multiply5.x, (IN.uv_Texture.xyxy).z, (IN.uv_Texture.xyxy).w);
			float4 Multiply1 = UV_Pan0 * _Tile.xxxx;
			float4 Tex2D0 = tex2D(_Texture, Multiply1.xy);
			float4 Multiply2 = Tex2D0 * _Strength.xxxx;
			float4 Multiply0 = Pow0 * Multiply2;
			float4 Multiply3 = Clamp0 * Multiply0;
			float4 Multiply4 = Saturate0 * Multiply3;

			//fixed4 reflcol = texCUBE(_Cube, IN.worldRefl);

			o.Emission = Multiply3.xyz * _Color.rgb;// + reflcol.rgb * _ReflectColor.rgb;
			o.Alpha = Multiply3.w * _Color.a;// + reflcol.a * _ReflectColor.a;
		}

		ENDCG


	} 
	FallBack "Diffuse"
}
