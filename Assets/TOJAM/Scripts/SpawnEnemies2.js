﻿#pragma strict

 public class enemyInWave
        {
        	public var enemy : GameObject;
        	public var spawnRate : float;
        	public var onlyAppearAfter : int;
        	public var onlyAppearBefore : int;
        	public var spawnsAtOnce : int;
        	public var timeSpentSinceBatch : float;
        }

public class Wave
    {
        public var enemies : enemyInWave[];
        public var timingBetweenSpawns : float;
        public var timeInWave : int;

    }

var cyclesThroughWaves : int;

var currentWave : int;
var realCurrentWave : int;

var timeSpentInWave : float;
var currentEnemyCounter : int;

var waves : Wave[];

var arenaSize : float;
var arenaSizeMin : float;

var waveComplete : boolean;
var waveCompleteTimer : float;

private var isFinished : boolean = false;

function Start () {

	waveComplete=false;
	waveCompleteTimer=-1.0;

}

function Update () {

	if (timeSpentInWave < waves[realCurrentWave].timeInWave)
	{
		timeSpentInWave+=Time.deltaTime;
		determineEnemySpawn();
	}
	else
	{
		checkForWaveCompletion();
		if (waveComplete == true)
		{
			waveCompleteTimer+=Time.deltaTime;
			if (waveCompleteTimer > 1.0)
			{
				waveCompleteTimer = 0.0;
				for (var j: int = 0; j < waves[realCurrentWave].enemies.length; j++)
				{
					waves[realCurrentWave].enemies[j].timeSpentSinceBatch = 0.0;
				}
				timeSpentInWave = 0.0;
				waveComplete = false;
				currentWave++;
				realCurrentWave++;


				if (realCurrentWave > (waves.length-1))
				{
					isFinished = true;
					FindObjectOfType(LevelManager).LoadNextLevel();
				}
			}
		}
	}

}

public function checkIsFinished() : boolean{
	return isFinished;
}

function checkForWaveCompletion()
{
	   var NMEs : GameObject[];
       NMEs = GameObject.FindGameObjectsWithTag("Enemy"); 
  
        if (NMEs.length == 0) 
        {
		waveComplete = true;
        }	
}

function determineEnemySpawn()
{
	for (var j: int = 0; j < waves[realCurrentWave].enemies.length; j++)
	{
		waves[realCurrentWave].enemies[j].timeSpentSinceBatch+=Time.deltaTime;
		if (waves[realCurrentWave].enemies[j].timeSpentSinceBatch > waves[realCurrentWave].enemies[j].spawnRate)
		{
			waves[realCurrentWave].enemies[j].timeSpentSinceBatch -= waves[realCurrentWave].enemies[j].spawnRate;

			for (var i : int = 0; i < waves[realCurrentWave].enemies[j].spawnsAtOnce; i++)
			{
				yield WaitForSeconds (0.2);
				createEnemy(waves[realCurrentWave].enemies[j].enemy);
				//Debug.Log(i+"/"+waves[realCurrentWave].enemies[j].spawnsAtOnce);
			}

		}
	}

}

function OnGUI()
{
	/*if (GUI.Button(Rect(0,0,200,100), "Wave: " + (currentWave+1) + "\n" + timeSpentInWave))
	{
		if ((realCurrentWave+1) < waves.length)
		{
			realCurrentWave++;
			currentWave++;
			timeSpentInWave=0;
		}
		//createEnemy(waves[0].enemies[0].enemy);
	}

	if (waveComplete == true && timeSpentInWave > waves[realCurrentWave].timeInWave)
	{
			if (GUI.Button(Rect(100,100,200,100), "WAVE COMPLETE"))
			{

			}
	}*/
}


function createEnemy(theEnemy: GameObject)
{
	var newEnemy : GameObject;

	newEnemy = Instantiate(theEnemy, determineDropLocation(), Quaternion.identity);
	if (newEnemy.name != "Basic Enemy(Clone)" && newEnemy.name != "Burly Unit(Clone)" && newEnemy.name != "WholeSnake(Clone)" && newEnemy.name != "Hunter Enemy(Clone)")
	{
	newEnemy.transform.localScale = Vector3(0.5,0.5,0.5);
	newEnemy.GetComponent(MoveTowardTarget).isSpawning = true;
	newEnemy.GetComponent(MoveTowardTarget).speed *= (cyclesThroughWaves+1);

	}
	else
	{

	newEnemy.transform.GetChild(0).renderer.material.color.a = 0.0;
	newEnemy.transform.GetChild(0).renderer.material.color.r += 1.0;
	newEnemy.transform.GetChild(0).renderer.material.color.g += 1.0;
	newEnemy.transform.GetChild(0).renderer.material.color.b += 1.0;

	newEnemy.transform.localScale = Vector3(0.5,0.5,0.5);
	newEnemy.transform.GetChild(0).GetComponent(MoveTowardTarget).isSpawning = true;
	newEnemy.transform.GetChild(0).GetComponent(MoveTowardTarget).speed *= (cyclesThroughWaves+1);

	}



}

function determineDropLocation() : Vector3
{
	var newEnemyPos : Vector3;

	newEnemyPos.x = Random.Range(-arenaSize,arenaSize);
	newEnemyPos.y = 0;
	newEnemyPos.z = Random.Range(-arenaSize,arenaSize);

	//Debug.Log(newEnemyPos);

	if (Mathf.Abs(newEnemyPos.x) < arenaSizeMin && (Mathf.Abs(newEnemyPos.z) < arenaSizeMin))
		return determineDropLocation();
	else
		return newEnemyPos;
}