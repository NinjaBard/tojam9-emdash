﻿#pragma strict

public var buttonName : String;

private var duration : float = 0;
private var recording : boolean = false;

function Start () {

}

function Update () {
	if (Input.GetButtonDown(buttonName)){
		recording = true;
		duration = 0;
	}

	if (recording){
		duration += Time.deltaTime;
	}

	if (Input.GetButtonUp(buttonName)){
		recording = false;
		Debug.Log(duration);
	}
}