﻿#pragma strict

public enum PlayerState { Idle = 0, Moving = 1, Charging = 2, Dodging = 4, Dashing = 8, PowerDashing = 16, Anchored = 32, Invincible = 64, DashCancel = 128 }

public var moveSpeed : float = 10.0;
private var dodgeSpeed : float = 25.0;
private var dashSpeed : float = 35.0;
private var dashThrownSpeed : float = 0.0;
private var dashThrownMultiplier : float = 1.4;
private var dashThrownMinSpeed : float = 42.0;
private var dashThrownMinDuration : float = 0.25;
public var playerNum : int = 0;
public var maxDashDuration : float = 0.55;
private var dashChargeSpeedMod : float = 0.5;
private var dashChargeMoveSpeedMod : float = 0.75;
public var anchorMoveSpeedMod : float = 0.5;
private var minDashTime : float = 0.24;
private var minChargeDashTime : float = 0.25;
private var dashCancelDuration : float = 0.18;
private var dashCancelDecay : float = 0.16;
public var rotationTime : float = 0.1;

private var dodgeDecayDuration : float = 0.15;
private var dodgeDecayMod : float = 0.1;
private var dashDecayDuration : float = 0.15;
private var dashDecayMod : float = 0.15;
private var powerDashDecayDuration : float = 0.2;
private var powerDashDecayMod : float = 0.25;

public var lastDudeHit : GameObject;
public var hitDudeAt : float = 0.0;
private var kickoffThreshold : float = 0.1;

public var dodgeDamage : int = 0;
public var dashDamage : int = 10;
public var powerDashDamage : int = 20;

public var buttonHeldDuration : float = 0;

private var dodgeCooldownTime : float = 0.9;
private var dashCooldownTime : float = 0.2;
private var dashCancelCooldownTime : float = 0.2;
public var dashCooldown : float = 0.0;

public var startCharge : float = 0.21;

public var invincibleTime : float = 0.3;
public var elapsedInvincibleTime : float;
public var shieldObject : GameObject;

public var lineRenderer : LineRenderer;
public var dashTrailRenderer : TrailRenderer;
public var powerDashTrailRenderer : TrailRenderer;
public var motionBlurTrailRenderer : TrailRenderer;

public var partSystem : ParticleSystem;

public var dashPartSystems : ParticleSystem[];

public var anchoredPartSystem : ParticleSystem;

public var shieldMat : Material;
private var defaultMat : Material;

@HideInInspector
public var state : int = PlayerState.Idle;

var direction : Vector3;
var controlDirection : Vector3;
var dashDirection : Vector3;

var playerMan : PlayerManager;
var playerCollider : Collider;

var dashDuration : float;
var elapsedDashTime : float;
var remainingDashTime : float;
private var dashDecay : float = 1;

var anchor : PlayerController = null;

var targetRotation : float;
var elapsedSlerpTime : float;
var shouldSlerp : boolean = false;

var damage : AttackingEntity;

public var animator : Animator;

function Start () {
	playerMan = FindObjectOfType(PlayerManager);

	playerCollider = gameObject.GetComponent(Collider);

	damage = gameObject.GetComponent(AttackingEntity);

	defaultMat = shieldObject.GetComponent(Renderer).material;
}

public function SetAnchored(isAnchor : boolean){
	if (isAnchor){
		AppendState(PlayerState.Anchored);

	} else {
		RemoveState(PlayerState.Anchored);
	}
}

function OnChargeStart(){
	SetState(PlayerState.Charging);

	animator.SetBool("Strafe", true);

	dashDuration = minChargeDashTime;

	partSystem.Play();

	RecalcAnchor();
}

function OnCharge(){
	dashDuration += Time.deltaTime * dashChargeSpeedMod;

	if (dashDuration >= maxDashDuration) dashDuration = maxDashDuration;

	RecalcAnchor();
}

function OnDodgeStart(){
	SetState(PlayerState.Dodging);

	animator.SetBool("Strafe", false);

	motionBlurTrailRenderer.time = 0.1;

	dashDuration = minDashTime;
	elapsedDashTime = 0;

	RecalcAnchor();
	dashDirection = Vector3.Normalize(direction);
	damage.damage = dodgeDamage;
}

function OnDashStart(){
	SetState(PlayerState.Dashing);

	animator.SetBool("Strafe", false);
	animator.SetBool("Dash", true);

	motionBlurTrailRenderer.time = 0.1;

	elapsedDashTime = 0;

	RecalcAnchor();
	dashDirection = Vector3.Normalize(anchor.transform.position - transform.position);
	damage.damage = dashDamage;

	dashTrailRenderer.time = 1;

	for (var i : int=0;i<dashPartSystems.Length;i++){
		dashPartSystems[i].Play();
	}
}

function OnHitDude(dude){
	hitDudeAt = Time.timeSinceLevelLoad;
	lastDudeHit = dude;
}

function OnPowerDashStart(){
	SetState(PlayerState.PowerDashing);

	animator.SetBool("Strafe", false);
	motionBlurTrailRenderer.time = 0.1;

	dashThrownSpeed = dashSpeed * dashDecay * dashThrownMultiplier;
	if (dashThrownSpeed < dashThrownMinSpeed) { dashThrownSpeed = dashThrownMinSpeed; }

	if (dashDuration - elapsedDashTime < dashThrownMinDuration) {
		dashDuration = elapsedDashTime + dashThrownMinDuration;
	}

	damage.damage = powerDashDamage;

	powerDashTrailRenderer.time = 1;
}

function OnCancel(){

	SetState(PlayerState.DashCancel);

	elapsedDashTime = 0;
	dashDuration = dashCancelDuration;

	dashDirection = Vector3.Normalize(dashDirection * -1);

	animator.SetBool("Strafe", false);
	animator.SetBool("DashCancel", true);
	animator.SetBool("Dash", false);

	if (anchor != null){
		anchor.SetAnchored(false);
		anchor = null;
	}

	dashTrailRenderer.time = 0;
	powerDashTrailRenderer.time = 0;

	for (var i : int=0;i<dashPartSystems.Length;i++){
		dashPartSystems[i].Stop();
	}
}

function OnInvincibleStart(){
	AppendState(PlayerState.Invincible);
	//shieldObject.SetActive(true);
	shieldObject.GetComponent(Renderer).material = shieldMat;
	elapsedInvincibleTime = 0;
}

function OnInvincibleEnd(){
	RemoveState(PlayerState.Invincible);
	//shieldObject.SetActive(false);
	shieldObject.GetComponent(Renderer).material = defaultMat;
}

function OnIdleStart(){
	if (IsState(PlayerState.Dodging)) {
		dashCooldown = dodgeCooldownTime;
	} else if (IsState(PlayerState.DashCancel)) {
		dashCooldown = dashCancelCooldownTime;
		direction = direction *= -1;
	} else {
		dashCooldown = dashCooldownTime;
	}

	animator.SetBool("Strafe", false);
	animator.SetBool("Dash", false);
	animator.SetBool("DashCancel", false);

	SetState(PlayerState.Idle);
	dashDirection = Vector3.zero;
	dashDuration = 0.0;
	dashTrailRenderer.time = 0;
	powerDashTrailRenderer.time = 0;
	motionBlurTrailRenderer.time = 0;

	for (var i : int=0;i<dashPartSystems.Length;i++){
		dashPartSystems[i].Stop();
	}
}

function OnHitObstacle(){
	animator.SetBool("Dash", false);
	animator.SetBool("DashCancel", false);
	SetState(PlayerState.Idle);
	dashDirection = Vector3.zero;
	dashCooldown = dashCooldownTime;
	dashDuration = 0.0;
	dashTrailRenderer.time = 0;
	powerDashTrailRenderer.time = 0;
	motionBlurTrailRenderer.time = 0;

	for (var i : int=0;i<dashPartSystems.Length;i++){
		dashPartSystems[i].Stop();
	}
}

function Update () {
	if (Input.GetButton("Dash" + playerNum)) {
		buttonHeldDuration += Time.deltaTime;
	} else if (Input.GetButtonUp("Dash" + playerNum)) {
		buttonHeldDuration = 0;
	}

	if (!IsDashing()){
		if (dashCooldown > 0.0) { dashCooldown -= Time.deltaTime; }
		if (dashCooldown <= 0.0) {
			if (Input.GetButtonDown("Dash" + playerNum)) {
				OnDodgeStart();
			}
			if (buttonHeldDuration >= startCharge && !IsState(PlayerState.Charging) && Input.GetButton("Dash" + playerNum)){ //first frame of charging dash
				OnChargeStart();
			} else if (buttonHeldDuration >= startCharge && Input.GetButton("Dash" + playerNum)){ //charging dash
				OnCharge();
			} else if (Input.GetButtonUp("Dash" + playerNum)){ //start dashing
				if (IsState(PlayerState.Charging)) {
					OnDashStart();
				}
			}
		}
	} else if ((IsState(PlayerState.Dashing) || IsState(PlayerState.PowerDashing)) && Input.GetButton("Dash" + playerNum)){
		if (Time.timeSinceLevelLoad - hitDudeAt < kickoffThreshold){
			OnCancel();
		}
	}

	if (IsDashing()){
		//collider.isTrigger = true;
		if (!IsState(PlayerState.Dodging)){
			animator.SetBool("Run", false);
		}
		gameObject.layer = 10;

		elapsedDashTime += Time.deltaTime;
		if (elapsedDashTime >= dashDuration){
			OnIdleStart();
		} else {
			remainingDashTime = dashDuration - elapsedDashTime;
			if (IsState(PlayerState.PowerDashing)) {
				if (remainingDashTime < powerDashDecayDuration) {
					animator.SetBool("Dash", false);
					dashDecay = powerDashDecayMod + (1 - powerDashDecayMod) * (1.0 - ((powerDashDecayDuration - remainingDashTime) / powerDashDecayDuration));
				} else {
					dashDecay = 1.0;
				}
				direction = dashDirection * dashThrownSpeed * dashDecay;
			} else if (IsState(PlayerState.DashCancel)){
				dashDecay = dashCancelDecay + (1 - dashCancelDecay) * (1.0 - ((dodgeDecayDuration - remainingDashTime) / dodgeDecayDuration));
				direction = dashDirection * dodgeSpeed * dashDecay;
			} else if (IsState(PlayerState.Dodging)){
				if (remainingDashTime < dodgeDecayDuration) {
					animator.SetBool("Dash", false);
					dashDecay = dodgeDecayMod + (1 - dodgeDecayMod) * (1.0 - ((dodgeDecayDuration - remainingDashTime) / dodgeDecayDuration));
				} else {
					dashDecay = 1.0;
				}
				direction = dashDirection * dodgeSpeed * dashDecay;
			} else {
				if (remainingDashTime < dashDecayDuration) {
					animator.SetBool("Dash", false);
					dashDecay = dashDecayMod + (1 - dashDecayMod) * (1.0 - ((dashDecayDuration - remainingDashTime) / dashDecayDuration));
				} else {
					dashDecay = 1.0;
				}
				direction = dashDirection * dashSpeed * dashDecay;
			}
		}

	} else {
		//collider.isTrigger = false;
		gameObject.layer = 9;

		controlDirection = Vector3(Input.GetAxis("Horizontal" + playerNum), 0, Input.GetAxis("Vertical" + playerNum));
		controlDirection.Normalize();
		if (IsState(PlayerState.Anchored)) {
			controlDirection *= moveSpeed * anchorMoveSpeedMod;
			if (anchoredPartSystem !=  null)
				anchoredPartSystem.Play();
		} else if (IsState(PlayerState.Charging)) {
			controlDirection *= moveSpeed * dashChargeMoveSpeedMod;
		} else {
			controlDirection *= moveSpeed;
			if (anchoredPartSystem !=  null)
			{
				anchoredPartSystem.Pause();
				anchoredPartSystem.Clear();
			}
		}

		direction += (controlDirection - direction) * 0.25;

		if (controlDirection != Vector3.zero){
			AppendState(PlayerState.Moving);
			animator.SetBool("Run", true);
		} else {
			RemoveState(PlayerState.Moving);
			animator.SetBool("Run", false);
		}

	}

	if (IsState(PlayerState.Invincible)){
		elapsedInvincibleTime += Time.deltaTime;
		if (elapsedInvincibleTime >= invincibleTime){
			OnInvincibleEnd();
		}
	}

	RemoveState(PlayerState.Anchored);
	rigidbody.velocity = Vector3.zero;
}

function LateUpdate(){
	direction.y = 0;
	transform.position += direction * Time.deltaTime;

	if (IsState(PlayerState.Charging)){
		var dir : Vector3 = Vector3.Normalize(anchor.transform.position - transform.position);
		targetRotation = -Mathf.Acos(Vector3.Normalize(dir).x) * 180/3.1415;
		if (dir.z < 0) targetRotation *=-1;

		elapsedSlerpTime = 0;
		shouldSlerp = true;
		//transform.localRotation = Quaternion.Euler(transform.localRotation.x, newRotation, transform.localRotation.z);
	} else if (direction != Vector3.zero){
		targetRotation = -Mathf.Acos(Vector3.Normalize(direction).x) * 180/3.1415;
		if (direction.z < 0) targetRotation *=-1;
		//transform.localRotation = Quaternion.Euler(transform.localRotation.x, newRot, transform.localRotation.z);
		elapsedSlerpTime = 0;
		shouldSlerp = true;
		if (IsState(PlayerState.DashCancel)){ shouldSlerp = false; }
	}

	if (shouldSlerp){
		elapsedSlerpTime += Time.deltaTime;

		if (elapsedSlerpTime >= rotationTime){
			shouldSlerp = false;
			elapsedSlerpTime = rotationTime;
		}
		transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(transform.localRotation.eulerAngles.x, targetRotation, transform.localRotation.eulerAngles.z), elapsedSlerpTime/rotationTime);
	}

	if (IsState(PlayerState.Charging)){
		animator.SetFloat("StrafeDir", (Input.GetAxis("Horizontal"+playerNum) > 0) ? 1 : -1);

		var tempDir : Vector3 = Vector3.Normalize(anchor.transform.position - transform.position);

		var firstPoint : Vector3 = transform.position + tempDir * dashDuration * dashSpeed;
		var secondPoint : Vector3 = firstPoint;
		var numLines : int = 2;

		var anchorDist : float = Mathf.Abs((anchor.transform.position - transform.position).magnitude);
		if (Mathf.Abs((firstPoint - transform.position).magnitude) >= anchorDist){
			firstPoint = anchor.transform.position;

			var newTime = dashDuration - anchorDist/dashSpeed;

			secondPoint = anchor.transform.position + tempDir * newTime * dashThrownSpeed;
			numLines = 3;
		}

		lineRenderer.SetVertexCount(numLines);
		lineRenderer.SetPosition(0, transform.position);
		lineRenderer.SetPosition(1, firstPoint);
		if (numLines == 3) lineRenderer.SetPosition(2, secondPoint);
	} else {
		lineRenderer.SetVertexCount(2);
		lineRenderer.SetPosition(0, transform.position);
		lineRenderer.SetPosition(1, transform.position + Vector3.Normalize(playerMan.GetClosestPlayerPos(transform.position, playerNum) - transform.position));
	}
}

function OnTriggerStay(collision : Collider){
	if (collision.gameObject.name.Contains("Player") && IsState(PlayerState.Dashing)){
		OnPowerDashStart();
	}
}

function OnCollisionEnter(collider : Collision){
	if (collider.gameObject.name.Contains("Wall")){
		OnHitObstacle();
	}
}

public function SetState(newState : PlayerState) {
	var isAnchor : boolean = IsState(PlayerState.Anchored);
	var isInvincible : boolean = IsState(PlayerState.Invincible);
	state = newState;

	if (isAnchor) AppendState(PlayerState.Anchored);
	if (isInvincible) AppendState(PlayerState.Invincible);
}

public function AppendState(newState : PlayerState){
	if (newState == PlayerState.DashCancel || newState == PlayerState.Dashing || newState == PlayerState.PowerDashing){
		SetState(newState);
	} else {
		state = state | parseInt(newState);
	}
}

public function RemoveState(newState : PlayerState){
	state = state & ~parseInt(newState);
}

public function IsState(checkState : PlayerState) : boolean {
	return (state & parseInt(checkState)) == parseInt(checkState);
}

public function IsDashing() : boolean {
	return IsState(PlayerState.Dodging) || IsState(PlayerState.Dashing) || IsState(PlayerState.PowerDashing) || IsState(PlayerState.DashCancel);
}

public function OnHealthChanged(newHP : int){
	OnInvincibleStart();
	animator.SetTrigger("Hit");
}

public function OnHealthSet(newHP : int){

}

function RecalcAnchor(){
	if (anchor != null) anchor.SetAnchored(false);
	anchor = playerMan.GetClosestPlayer(transform.position, playerNum);
	anchor.SetAnchored(true);
}
