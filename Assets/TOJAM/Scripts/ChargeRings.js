﻿#pragma strict

private var player : GameObject;
private var playerController: PlayerController;
private var chargeFill : GameObject;
private var chargeRings : ParticleSystem;
private var indicatorVisible : boolean = false;
public var originalScale : Vector3;

function Start () {
  player = transform.parent.gameObject;
  Debug.Log(player);
  playerController = player.GetComponent("PlayerController");
  chargeFill = transform.Find("ChargeFill").gameObject;
  chargeRings = gameObject.GetComponent(ParticleSystem);
  originalScale = transform.localScale;
}

function Update () {
  if (gameObject.renderer.enabled != 0 < playerController.buttonHeldDuration) {
    indicatorVisible = 0 < playerController.buttonHeldDuration;
    if (playerController.IsDashing()){ indicatorVisible = false; }
    gameObject.renderer.enabled = indicatorVisible;
    chargeFill.renderer.enabled = indicatorVisible;
  }

  if (playerController.buttonHeldDuration < playerController.startCharge && playerController.dashCooldown <= 0){
    transform.localScale = originalScale * (((1 - (playerController.startCharge - playerController.buttonHeldDuration)) * 0.8) + 0.2);
  }

  if (playerController.IsState(PlayerState.Charging)){
    transform.localScale = originalScale;
    chargeFill.transform.localScale = Vector3(1,1,1) * (playerController.dashDuration / playerController.maxDashDuration);

    if (playerController.dashDuration / playerController.maxDashDuration == 1){
      chargeRings.Play();
    }
  } else {
    if (!chargeRings.isPaused){
      chargeRings.Pause();
      chargeRings.Clear();
    }
    chargeFill.transform.localScale = Vector3(0,0,0);
  }

  //chargeFill.transform.localScale = 100;

}
