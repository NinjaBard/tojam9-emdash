﻿using UnityEngine;
using System.Collections;

public class NGUIFadeOut : MonoBehaviour {

	public float duration = 0.1f;

	float elapsedTime = 0;
	bool shouldFade = false;

	void Start () {
	
	}

	void Update () {
		if (shouldFade){
			elapsedTime += Time.deltaTime;

			if (elapsedTime >= duration){
				elapsedTime = duration;
				shouldFade = false;
			}

			gameObject.GetComponent<UIPanel>().alpha = 1.0f - elapsedTime / duration;
		}
	}

	public void FadeOut(){
		shouldFade = true;
	}
}
