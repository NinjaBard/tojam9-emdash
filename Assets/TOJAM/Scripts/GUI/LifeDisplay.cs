﻿using UnityEngine;
using System.Collections;

public class LifeDisplay : MonoBehaviour {

	public UILabel label;
	
	int hp;

	void Start () {
	
	}

	void Update () {
		label.text = gameObject.name + ": " + hp;
	}

	void OnHealthChanged(int newHealth){
		hp = newHealth;
	}

	void OnHealthSet(int newHealth){
		hp = newHealth;
	}
}
