﻿#pragma strict

private var curChunk = 1;

private var hitCount = 0;

function Start () {

}

function Update () {

}

public function GetChunk() : int {
	return curChunk;
}

public function RemoveHealth(){
	if (curChunk <= 5){
		hitCount++;
		if (hitCount >= 3){
			var hp : GameObject = GameObject.Find("Health" + curChunk);
			Debug.Log(hp.name);

			hp.SendMessage("FadeOut");

			curChunk++;

			if (curChunk > 5){
				FindObjectOfType(LevelManager).EndGame();
			}
			hitCount = 0;
		}
	}
}