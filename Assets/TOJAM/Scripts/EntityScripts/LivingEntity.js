﻿#pragma strict

public var hp : int;

private var alive : boolean = true;

public var damageParticleSystem: ParticleSystem;

function Start () {
	gameObject.SendMessage("OnHealthSet", hp);
}

function Update () {
	if (alive && hp <= 0){
		alive = false;
		gameObject.SendMessage("OnDeath");
	}
}

public function ModHP(mod : int){
	hp += mod;
	gameObject.SendMessage("OnHealthChanged", hp);
	
	if(GetComponent(Renderer))
	{

	}

	if (alive && hp <= 0){
		alive = false;
		gameObject.SendMessage("OnDeath");
	}
	else
	{
	if (damageParticleSystem != null)
		damageParticleSystem.Play();
	}

}