﻿#pragma strict

public var velocityDecay : float = 0.9;
public var spinDecay : float = 0.9;

function Start () {

}

function Update () {
	rigidbody.velocity *= velocityDecay;
	rigidbody.angularVelocity *= spinDecay;
}