﻿#pragma strict

var players;
var checkFrequency : float;
var checkCounter : float;

function Start () {

}

function Update () {

checkCounter += Time.deltaTime;
if (checkCounter > checkFrequency)
	{
	findClosestPlayer();
	checkCounter-=checkFrequency;
	}
}

function findClosestPlayer ()
{
		// Find all game objects with tag Enemy
		var players : GameObject[];
		players = GameObject.FindGameObjectsWithTag("Player"); 
		var closest : GameObject; 
		var distance = Mathf.Infinity; 
		var position = transform.position; 

		if (players.Length > 0){
			// Iterate through them and find the closest one
			for (var player : GameObject in players)  { 
				var diff = (player.transform.position - position);
				var curDistance = diff.sqrMagnitude; 
				if (curDistance < distance) { 
					closest = player; 
					distance = curDistance; 
				} 
			} 

			if (GetComponent(MoveTowardTarget))
			{
				GetComponent(MoveTowardTarget).target.x = closest.transform.position.x;
				GetComponent(MoveTowardTarget).target.y = closest.transform.position.z;	
			}
		}
}