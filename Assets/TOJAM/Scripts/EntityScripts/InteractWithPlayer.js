﻿#pragma strict

private var hp : LivingEntity;
private var damage : AttackingEntity;

public var burly: boolean;

var hitCount : int;

function Start () {
	hp = gameObject.GetComponent(LivingEntity);
	damage = gameObject.GetComponent(AttackingEntity);
}

function Update () {

}

function OnTriggerEnter (player : Collider) {

	var cont : PlayerController = player.transform.parent.gameObject.GetComponent(PlayerController);

	if (player.gameObject.tag=="PlayerLauncher" && cont.IsDashing()){

		if (gameObject.tag == "BounceAway")
		{
			Debug.Log("Bounce Away Dash");
		}
		var dir : Vector3 = Vector3.Normalize(gameObject.transform.position - player.gameObject.transform.position);
		rigidbody.AddForce(dir * 2000);
		if (burly == true) {
			if 	(player.transform.parent.gameObject.GetComponent(PlayerController).state == PlayerState.PowerDashing) {
				hp.ModHP(-cont.gameObject.GetComponent(AttackingEntity).damage);
			} else {
				//Debug.Log("No power damage");
			}
		} else {
			hp.ModHP(-cont.gameObject.GetComponent(AttackingEntity).damage);
			cont.OnHitDude(gameObject);
		}
	}

}

function OnCollisionEnter (player : Collision) {


	if (player.gameObject.GetComponent(PlayerController))
		{
		var cont : PlayerController = player.gameObject.GetComponent(PlayerController);

			if (cont.IsDashing() && gameObject.tag == "BounceAway")
			{
					Debug.Log("Bounce Away Dash");
					player.gameObject.GetComponent(PlayerController).dashDirection *= -0.5;
			}
			else
			{

				if (player.gameObject.tag=="Player" && !cont.IsDashing() && !cont.IsState(PlayerState.Invincible))
				{
					//player.gameObject.GetComponent(LivingEntity).ModHP(-damage.damage);
					
					FindObjectOfType(HealthDisplay).RemoveHealth();
					cont.OnHealthChanged(0);
					if (gameObject.name == "Leap Enemy" || gameObject.name == "Leap Enemy(Clone)")
					{
						Debug.Log("IS LEAPER");
						GetComponent(MoveTowardTarget).distanceModifier = 0.0;
						GetComponent(MoveTowardTarget).alterRetargetingFrequency(2.0);
						GetComponent(MoveTowardTarget).changeDirectionTimer = 3.0;
						var dir2 : Vector3 = Vector3.Normalize(gameObject.transform.position - player.gameObject.transform.position);
						rigidbody.AddForce(dir2 * 200);
					}
					else
					{

					}
				}
			}
		}
	}

function OnHealthChanged(newHP : int){

}

function OnHealthSet(newHP : int){

}
