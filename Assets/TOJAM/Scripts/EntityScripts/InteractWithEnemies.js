﻿#pragma strict

function Start () {

}

function Update () {

}

function OnCollisionEnter (enemy : Collision) {
	if (enemy.gameObject.tag=="Enemy")
	{
		//Destroy(this.gameObject);
		if (enemy.gameObject.GetComponent(MoveTowardTarget))
		{
			enemy.gameObject.GetComponent(MoveTowardTarget).isDead = true;
			if (enemy.gameObject.GetComponent(BoxCollider))
				Destroy(enemy.gameObject.GetComponent(BoxCollider));
		}
	}
}