﻿#pragma strict

public var deathAnim : String;

public var animCtrl : Animator;

public var deathParticleSystem : ParticleSystem;


function Start () {

}

function Update () {

}

function OnDeath(){
	if (animCtrl != null){
		animCtrl.Play(deathAnim);
	}

	if (GetComponent(MoveTowardTarget) && gameObject.tag=="Enemy")
	{
		GetComponent(MoveTowardTarget).deathRotation = Vector3(Random.Range(-1.0,1.0),Random.Range(-1.0,1.0),Random.Range(-1.0,1.0));
		GetComponent(MoveTowardTarget).isDead = true;

			//deathParticleSystem.Play();

		if (deathParticleSystem != null)
			deathParticleSystem.Play();

	}
	//Destroy(gameObject);
}