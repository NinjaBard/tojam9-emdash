﻿#pragma strict

var players;
var checkFrequency : float;
var checkCounter : float;

var playerName : String;

function Start () {

pickPlayerAtRandom();
}

function Update () {

checkCounter += Time.deltaTime;
if (checkCounter > checkFrequency)
	{
	findClosestPlayer();
	checkCounter-=checkFrequency;
	}
}

function findClosestPlayer ()
{
		// Find all game objects with tag Enemy
		var players : GameObject[];
		players = GameObject.FindGameObjectsWithTag("Player"); 
		var closest : GameObject; 
		var distance = Mathf.Infinity; 
		var position = transform.position; 

		if (players.Length > 0){
			// Iterate through them and find the closest one
			for (var player : GameObject in players)  { 
				var diff = (player.transform.position - position);
				var curDistance = diff.sqrMagnitude; 
				if (player.name == playerName) { 
					closest = player; 
					distance = curDistance; 
				} 
			} 

			if (GetComponent(MoveTowardTarget))
			{
				GetComponent(MoveTowardTarget).target.x = closest.transform.position.x;
				GetComponent(MoveTowardTarget).target.y = closest.transform.position.z;	
			}
		}
}

function pickPlayerAtRandom()
{
	var numPlayers : GameObject[];
	numPlayers = GameObject.FindGameObjectsWithTag("Player"); 

	playerName = "Player";

	var determine : float;
	determine = Random.Range(0,1.0);

	if (numPlayers.length == 1)
		playerName+="1";
	else if (numPlayers.length == 2)
	{
		if (determine >= 0.5)
			playerName+="1";
		else
			playerName+="2";
	}
	else if (numPlayers.length == 3)
	{

		if (determine <= 3.33)
			playerName+="1";
		else if (determine >=6.66)
			playerName+="2";
		else
			playerName+="3";
	}
	else if (numPlayers.length == 4)
	{

		if (determine <= 0.25)
			playerName+="1";
		else if (determine >= 0.25 && determine <= 0.50)
			playerName+="2";
		else if (determine >= 0.5 && determine <= 0.75)
			playerName+="3";
		else
			playerName+="4";
	}
	playerName+="(Clone)";




}