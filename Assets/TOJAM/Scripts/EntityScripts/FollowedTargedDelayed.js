﻿#pragma strict

var target: GameObject;
var direction : Vector3;
var oldDirection : Vector3;


var checkFrequency : float;
var movementDelay : float;

var snakeRotateTimer : float;
var rotateLR : boolean;

function Start () {

direction = Vector3(0,0,0);
oldDirection = Vector3(0,0,0);
transform.position = target.transform.position;

}

function Update () {

if (target!=null)
{
	if (movementDelay == 0)
	{
		//if (Mathf.Abs((transform.position - target.transform.position).magnitude) < 0.01)
		//{
		//	movementDelay += checkFrequency;
		//	Debug.Log("close enough");
		//}
		//else
		//{
			if (target.GetComponent(MoveTowardTarget))
			{
				oldDirection = direction;
				direction = target.GetComponent(MoveTowardTarget).direction;
			}
			movementDelay += checkFrequency;
		//}
	}
	else
	{
		GetComponent(MoveTowardTarget).direction = oldDirection;
	}

	//transform.position = target.transform.position;

	movementDelay -= Time.deltaTime;
	if (movementDelay < 0)
		movementDelay = 0;

	snakeRotateTimer += 1.0;
	if (snakeRotateTimer == 30.0)
		determineRotationDirection();
	if (snakeRotateTimer > 30.0)
		snakeRotate();
	else
		snakeStabilize();
	if (snakeRotateTimer > 60.0)
		snakeRotateTimer = 0;

	easeToMasterAxis();
	//snakeRotate();
}
else
{
		gameObject.SendMessage("OnDeath");
}

}

function easeToMasterAxis()
{
	if (Mathf.Abs(target.GetComponent(MoveTowardTarget).direction.x) >= Mathf.Abs(target.GetComponent(MoveTowardTarget).direction.y))
	{
		if (transform.position.z != target.transform.position.z)
		{
			transform.position.z += (target.transform.position.z - transform.position.z)*0.05;
		}
	}
	else
	{
		if (transform.position.x != target.transform.position.x)
		{
			transform.position.x += (target.transform.position.x - transform.position.x)*0.05;
		}
	}

}

function snakeRotate()
{
	if (target.GetComponent(MoveTowardTarget))
	{
		if (rotateLR == true)
		{
			transform.Rotate(12,0,0);
		}
		else
		{
			transform.Rotate(0,0,12);
		}

	}
}

function determineRotationDirection()
{
	if (Mathf.Abs(target.GetComponent(MoveTowardTarget).direction.x) >= Mathf.Abs(target.GetComponent(MoveTowardTarget).direction.y))
		{
			rotateLR = true;
		}
		else
		{
			rotateLR = false;
		}

}

function snakeStabilize()
{
	if (GetComponent(MoveTowardTarget))
	{
		transform.rotation = Quaternion.identity;
	}
}