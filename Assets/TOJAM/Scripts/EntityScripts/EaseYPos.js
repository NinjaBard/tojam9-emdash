﻿#pragma strict

var startY : float;

function Start () {
	startY = transform.position.y;
}

function Update () {
	if (transform.position.y > startY)
		transform.Translate(0,-0.05,0);
	else if (transform.position.y < startY)
		transform.position.y = startY;
}