﻿#pragma strict

var speed: float;
var direction : Vector2;
var target : Vector2;

var acceleratesIfClose : boolean;
var distanceThreshold : float;
var boostModifier : float;


var isDead : boolean;
var isSpawning : boolean;

var deathRotation : Vector3;


function Start () {

isDead = false;

target.x=0;
target.y=0;
}

function Update () {

	if (isDead == true)
	{
		renderer.material.color.a -= 0.05;
		renderer.material.color.r += 0.1;
		renderer.material.color.g += 0.1;
		renderer.material.color.b += 0.1;

		transform.localScale -= Vector3(0.035,0.035,0.035);

		if (transform.localScale.x < 0.25)
			transform.localScale.x = 0.25;
		if (transform.localScale.y < 0.25)
			transform.localScale.y = 0.25;
		if (transform.localScale.z < 0.25)
			transform.localScale.z = 0.25;

		transform.Rotate(deathRotation.x, deathRotation.y, deathRotation.z);

		if (renderer.material.color.a < -1.0)
			Destroy(this.gameObject);

	}
	else if (isSpawning == true)
	{
		renderer.material.color.a += 0.05;
		renderer.material.color.r -= 0.06;
		renderer.material.color.g -= 0.06;
		renderer.material.color.b -= 0.06;

		if (renderer.material.color.a >= 1.0)
		{
			renderer.material.color.a = 1.0;
			isSpawning=false;
		}

	}
	else
	{

	direction.x = target.x-transform.position.x;
	direction.y = target.y-transform.position.z;
	direction.Normalize();
	var distanceModifier : float;
	
	if (acceleratesIfClose)
	{
		var distanceBetween: float;
		distanceBetween = (Mathf.Abs((target.x-transform.position.x)) + Mathf.Abs((target.y-transform.position.z)))/2;
		if (distanceBetween < 1.0)
		{
			distanceModifier = 0.0;
			alterRetargetingFrequency(2.0);
		}
		else if (distanceBetween < distanceThreshold)
		{
			distanceModifier = boostModifier*1.5;
			alterRetargetingFrequency(2.0);
		}
		else 
		{
			distanceModifier = 1.0;
			alterRetargetingFrequency(0.05);
		}

		}
		else
		{
			distanceModifier = 1.0;
		}

		transform.position.x += direction.x*speed*distanceModifier*Time.deltaTime;
		transform.position.z += direction.y*speed*distanceModifier*Time.deltaTime;
	}
}

function alterRetargetingFrequency(newFrequency : float)
{
	if (GetComponent(ChaseNearestPlayer))
	{
		GetComponent(ChaseNearestPlayer).checkFrequency = newFrequency;
	}
}