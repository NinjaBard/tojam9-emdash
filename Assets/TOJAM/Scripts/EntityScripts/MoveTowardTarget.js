﻿#pragma strict

var speed: float;
var direction : Vector2;
var target : Vector2;

var acceleratesIfClose : boolean;
var rightTurnsOnly : boolean;
var changeDirectionTimer : float;

var distanceThreshold : float;
var boostModifier : float;
var distanceModifier : float;

var isDead : boolean;
var isSpawning : boolean;

var deathRotation : Vector3;

var miscParticleSystem : ParticleSystem;

function Start () {

isDead = false;

target.x=0;
target.y=0;
}

function Update () {

	if (isDead == true)
	{
		//if (miscParticleSystem != null)
		//	miscParticleSystem.Stop();

		fadeAllChildren(this.transform);

		transform.localScale -= Vector3(0.05,0.05,0.05);

		if (transform.localScale.x < 0.1)
			transform.localScale.x = 0.1;
		if (transform.localScale.y < 0.1)
			transform.localScale.y = 0.1;
		if (transform.localScale.z < 0.1)
			transform.localScale.z = 0.1;

		transform.Rotate(deathRotation.x, deathRotation.y, deathRotation.z);

		if (transform.renderer.material.color.a < -1.0)
			Destroy(this.gameObject);

	}
	else if (isSpawning == true)
	{
		transform.renderer.material.color.a += 0.05;
		transform.renderer.material.color.r -= 0.06;
		transform.renderer.material.color.g -= 0.06;
		transform.renderer.material.color.b -= 0.06;

		if (transform.parent != null)
		{
		transform.parent.transform.localScale += Vector3(0.05,0.05,0.05);
		if (transform.parent.transform.localScale.x > 1.0)
			transform.parent.transform.localScale = Vector3(1,1,1);
		}
		else
		{
		Debug.Log("NO PARENT");
		transform.localScale += Vector3(0.05,0.05,0.05);
		if (transform.localScale.z > 1.0)
			transform.localScale = Vector3(1,1,1);
		}


		if (transform.renderer.material.color.a >= 1.0)
		{
			transform.renderer.material.color.a = 1.0;
			isSpawning=false;
		}

	}
	else
	{
	if (GetComponent(FollowedTargedDelayed))
	{
		moveDirection(1.0);
		Debug.Log("Has direction");
	}
	else
		determineDirection();
	}
}

function fadeAllChildren(child : Transform)
{
	//Debug.Log (child.gameObject.name);
	if (child.gameObject.name !=  "Death Particles" && child.gameObject.name !=  "Enemy Large")
	{
	child.gameObject.renderer.material.color.a -= 0.05;
	child.gameObject.renderer.material.color.r += 0.1;
	child.gameObject.renderer.material.color.g += 0.1;
	child.gameObject.renderer.material.color.b += 0.1;

	for (var children : Transform in child.transform) 
    	{
    		fadeAllChildren(children);
    	}
    }
}

function alterRetargetingFrequency(newFrequency : float)
{
	if (GetComponent(ChaseNearestPlayer))
	{
		GetComponent(ChaseNearestPlayer).checkFrequency = newFrequency;
	}
}

function determineDirection()
{
	changeDirectionTimer -= Time.deltaTime;
	if (changeDirectionTimer < 0)
		changeDirectionTimer = 0;

	if (changeDirectionTimer == 0) 
	{
	direction.x = target.x-transform.position.x;
	direction.y = target.y-transform.position.z;

	if (rightTurnsOnly)
	{
		//alterRetargetingFrequency(1.0);
		//Debug.Log(direction);
		changeDirectionTimer += 0.85;
		if (Mathf.Abs(direction.x) >= Mathf.Abs(direction.y))
		{
			direction.y = 0;
			//Debug.Log("X is greater");
		}
		else
		{
			direction.x = 0;
			//Debug.Log("Y is greater");
		}

	}
	direction.Normalize();

		if (acceleratesIfClose)
		{
			var distanceBetween: float;
			distanceBetween = (Mathf.Abs((target.x-transform.position.x)) + Mathf.Abs((target.y-transform.position.z)))/2;
			if (distanceBetween < 0.5)
			{
				distanceModifier = 0.0;
				alterRetargetingFrequency(2.0);
			}
			else if (distanceBetween < distanceThreshold)
			{
				distanceModifier = boostModifier*1.5;
				alterRetargetingFrequency(2.0);
			}
			else 
			{
				distanceModifier = 1.0;
				alterRetargetingFrequency(0.05);
			}

		}
		else
		{
			distanceModifier = 1.0;
		}
	}
	else
	{
		//distanceModifier = 1.0;
	}

	//Debug.Log("it is moving   " + direction);

	moveDirection(distanceModifier);
}

function moveDirection(distanceModifier2: float)
{
	transform.position.x += direction.x*speed*distanceModifier2*Time.deltaTime;
	transform.position.z += direction.y*speed*distanceModifier2*Time.deltaTime;
}