﻿#pragma strict

private var players : GameObject[];
private var numPlayers : int;

function Start () {
	
}

public function FindPlayers(){
	var p : PlayerController[] = FindObjectsOfType(PlayerController);

	numPlayers = p.Length;
	players = new GameObject[numPlayers];

	for (var i=0;i<p.Length;i++){
		players[i] = p[i].gameObject;
	}
}

function Update () {

}

public function GetClosestPlayerPos(target : Vector3, playerNum : int) : Vector3 {
	var curDist : float;
	var distSet : boolean = false;
	var out : Vector3 = Vector3.zero;

	for (var i=0;i<numPlayers;i++){
		var tempDist : float = Mathf.Abs((players[i].transform.position - target).magnitude);
		if (players[i].GetComponent(PlayerController).playerNum != playerNum && (!distSet || tempDist < curDist)){
			curDist = tempDist;
			distSet = true;
			out = players[i].transform.position;
		}
	}

	return out;
}

public function GetClosestPlayer(target : Vector3, playerNum : int) : PlayerController {
	var curDist : float;
	var distSet : boolean = false;
	var out : PlayerController = null;

	for (var i=0;i<numPlayers;i++){
		var tempDist : float = Mathf.Abs((players[i].transform.position - target).magnitude);
		if (players[i].GetComponent(PlayerController).playerNum != playerNum && (!distSet || tempDist < curDist)){
			curDist = tempDist;
			distSet = true;
			out = players[i].GetComponent(PlayerController);
		}
	}

	return out;
}
