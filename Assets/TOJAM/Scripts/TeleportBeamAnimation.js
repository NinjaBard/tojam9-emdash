﻿#pragma strict

public var target : GameObject;
public var targetScale : float;
public var growthSpeed : float;
public var shouldAnim : boolean = false;

private var startScale : float;
private var scaleDif : float;
private var startY : float;

private var goingDown : boolean;

function Start () {
	startScale = target.transform.localScale.y;
	startY = target.transform.localPosition.y;

	goingDown = true;
}

function Update () {
	if (shouldAnim){

		if (goingDown){
			scaleDif = growthSpeed * Time.deltaTime;
			target.transform.localScale += Vector3(0,scaleDif,0);

			target.transform.localPosition -= Vector3(0, scaleDif, 0);
		} else {
			//target.transform.localScale += 
		}

		if (target.transform.localScale.y >= targetScale){
			if (goingDown){
				goingDown = false;
			} else {
				shouldAnim = false;
			}
		}
	}
}

public function StartAnim(){
	shouldAnim = true;
}