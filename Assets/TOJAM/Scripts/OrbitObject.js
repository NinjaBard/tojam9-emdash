﻿#pragma strict

public var target : GameObject;
public var orbitSpeed : float;
public var axis : Vector3;

function Start () {
	axis.Normalize();
}

function Update () {
	var frameRot : float = orbitSpeed * Time.deltaTime;

	transform.RotateAround(target.transform.position, axis, frameRot);
}