﻿#pragma strict

var mat : Material;

function Start () {
	mat = renderer.material;
}

function Update () {
	var newRim : float = (Mathf.Sin(Time.timeSinceLevelLoad * 10) + 1.0) / 2.0 + 1.2;
	mat.SetFloat("_Rim", newRim);
}