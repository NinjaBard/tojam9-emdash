﻿#pragma strict

public var player1Prefab : GameObject;
public var player2Prefab : GameObject;

public var spawnAnim : GameObject;
public var spawnAnimP2 : GameObject;

public var leaveAnim : GameObject;
public var leaveAnimP2 : GameObject;

public var levelList : String[];

private var curLevel : int;

private var playersSpawned : boolean;
private var playersLeft : boolean;
private var player1 : GameObject;
private var player2 : GameObject;

function Awake(){
	DontDestroyOnLoad(gameObject);
}

function Start () {
	curLevel = -1;
	playersSpawned = false;
}

function Update () {
}

public function EndGame(){
	playersSpawned = false;
	curLevel = -1;
	Application.LoadLevel("GameOver");

	player1.GetComponentInChildren(Animator).SetTrigger("Dead");
	player2.GetComponentInChildren(Animator).SetTrigger("Dead");
}

public function LoadNextLevel(){
	playersSpawned = false;
	curLevel++;
	Debug.Log(curLevel);

	if (curLevel >= levelList.Length){
		Application.LoadLevel("WinScreen");
		curLevel = -1;
	} else {
		Application.LoadLevel(levelList[curLevel]);
	}
}

public function TeleportOut(){
	if (!playersLeft){
		var spawnPoints : GameObject[] = GameObject.FindGameObjectsWithTag("SpawnPoint");

		if (spawnPoints.Length < 2){
			Debug.LogError("Not enough spawn points bro. We need at least 2.");
			return;
		}

		var go = Instantiate(leaveAnim);
		go.transform.parent = GameObject.Find("Tilt").transform;
		go.transform.localPosition = player1.transform.localPosition;
		go.transform.localRotation = player1.transform.localRotation;
		Destroy(player1);

		go.GetComponent(Animation).Play();

		go = Instantiate(leaveAnimP2);
		go.transform.parent = GameObject.Find("Tilt").transform;
		go.transform.localPosition = player2.transform.localPosition;
		go.transform.localRotation = player2.transform.localRotation;
		Destroy(player2);

		go.GetComponent(Animation).Play();
		playersLeft = true;
	}
}

public function SpawnPlayers(){
	if (!playersSpawned){
		var spawnPoints : GameObject[] = GameObject.FindGameObjectsWithTag("SpawnPoint");

		if (spawnPoints.Length < 2){
			Debug.LogError("Not enough spawn points bro. We need at least 2.");
			return;
		}

		var go = Instantiate(spawnAnim);
		go.transform.parent = GameObject.Find("Tilt").transform;
		go.transform.localPosition = spawnPoints[0].transform.localPosition;
		go.transform.localRotation = Quaternion.Euler(0,180,0);

		go.GetComponent(Animation).Play();

		go = Instantiate(spawnAnimP2);
		go.transform.parent = GameObject.Find("Tilt").transform;
		go.transform.localPosition = spawnPoints[1].transform.localPosition;
		go.transform.localRotation = Quaternion.Euler(0,180,0);

		go.GetComponent(Animation).Play();
		playersSpawned = true;
	}
}

public function LegitSpawn(id : int){
	var spawnPoints : GameObject[] = GameObject.FindGameObjectsWithTag("SpawnPoint");

	if (spawnPoints.Length < 2){
		Debug.LogError("Not enough spawn points bro. We need at least 2.");
		return;
	}

	var tilt : GameObject = GameObject.Find("Tilt");

	if (id == 1){

		player1 = Instantiate(player1Prefab);
		player1.transform.parent = tilt.transform;
		player1.transform.localPosition = spawnPoints[0].transform.localPosition;
		player1.transform.localRotation = Quaternion.Euler(0,90,0);

		FindObjectOfType(PlayerManager).FindPlayers();
	}

	if (id == 2){
		player2 = Instantiate(player2Prefab);
		player2.transform.parent = tilt.transform;
		player2.transform.localPosition = spawnPoints[1].transform.localPosition;
		player2.transform.localRotation = Quaternion.Euler(0,90,0);
		FindObjectOfType(PlayerManager).FindPlayers();
	}
}